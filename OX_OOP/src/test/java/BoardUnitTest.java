/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.ox_oop.Board;
import com.mycompany.ox_oop.Player;

/**
 *
 * @author Golf
 */
public class BoardUnitTest {
    
    public BoardUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    public void testRow1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setTable(1, 1);
        board.setTable(1, 2);
        board.setTable(1, 3);
        assertEquals(true, board.checkWin());
    }
    public void testCol1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setTable(1, 1);
        board.setTable(2, 1);
        board.setTable(3, 1);
        assertEquals(true, board.checkWin());
    }
    public void testSwitchPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.switchTurn();
        assertEquals('x',board.getCurrentPlayer().getName());
    }
    
}
