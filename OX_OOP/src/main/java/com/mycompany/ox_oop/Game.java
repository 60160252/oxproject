package com.mycompany.ox_oop;


import java.util.*;

public class Game {

    private Board board;
    private int row;
    private int col;
    private Player o;
    private Player x;

    public Game() {
        o = new Player('O');
        x = new Player('X');
    }

    public void startGame() {
        board = new Board(o, x);
        showWelcome();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(board.isFinish()){
                break;
            }
            board.switchTurn();
        }
        showTable();
        showWinner();
        showStat();

    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game!!");
    }

    public void showTable() {
        char[][] table = board.getTable();

        System.out.println(" 1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print((i + 1));
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(" " + table[i][j]);
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println(board.getCurrentPlayer().getName() + " Turn");
    }

    public void inputRowCol() {
        Scanner as = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("Please input Row Col: ");
                String input = as.nextLine();
                String[] str = input.split(" ");
                if (str.length != 2) {
                    System.out.println("Table is not empty!");
                    continue;
                }
                break;
            } catch (Exception e) {
                System.out.println("Please input Rol Col [1-3]");
                continue;
            }
        }

    }
    
    public void showWinner(){
        Player player = board.getWinner();
        System.out.println(player.getName()+ " Win!!");
    }
    
    public void showStat(){
        System.out.println(o.getName()+" "+"W,L,D: ");
        System.out.println(" "+ o.getWin()+ ","+ o.getLose()+","+o.getDraw());
        
        System.out.println(x.getName()+" "+"W,L,D: ");
        System.out.println(" "+ x.getWin()+ ","+ x.getLose()+","+x.getDraw());
    }
    
    public void Run(){
        while(true){
            startGame();
        }
    }
}
